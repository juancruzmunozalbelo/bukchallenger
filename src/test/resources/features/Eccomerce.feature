Feature: Registrar demoblaze

  @Demo
  Scenario Outline: Hacer registro valido/user exist en demoblaze
    Given el usuario esta en el home de demoblaze.com
    When el usuario hace click en el boton SingUp
    And el usuario ingresa los campos. Username: <user> Password: <pass>
    And el usuario hace click en el boton SingUpModal
    Then el usuario verifica el <message> de alerta
    And el usuario acepta la alerta
    Examples:
      | user                              | pass     | message                  |
      | juancruzmunozalbelotest@gmail.com | 12345678 | This user already exist. |
      | prueba@gmail.com                  | 12345678 | Sign up successful.      |
