Feature: Login demoblaze

  @Demo
  Scenario Outline: Hacer login valido en demoblaze
    Given el usuario esta en el home de demoblaze.com
    When el usuario hace click en el boton Login
    And el usuario ingresa los campos login. Username: <user> Password: <pass>
    And el usuario hace click en el boton LoginModal
    Then el usuario verifica la presencia de : <user> de la sesion
    Examples:
      | user                              | pass     |
      | juancruzmunozalbelotest@gmail.com | 12345678 |


  Scenario Outline: Hacer login invalido en demoblaze
    Given el usuario esta en el home de demoblaze.com
    When el usuario hace click en el boton Login
    And el usuario ingresa los campos login. Username: <user> Password: <pass>
    And el usuario hace click en el boton LoginModal
    Then el usuario verifica el <message> de alerta
    And el usuario acepta la alerta

    Examples:
      | user                              | pass | message
      | juancruzmunozalbelotest@gmail.com | fail | Wrong password.