Feature: Compra demoblaze

  @Demo
  Scenario Outline: Hacer una compra valida en demoblaze
    Given el usuario esta en el home de demoblaze.com
    When el usuario hace click en el item: <id>
    And el usuario hace click en el boton AddToCart
    And el usuario acepta la alerta
    And el usuario hace click en el boton HOME
    And el usuario hace click en un adicional item: <secondId>
    And el usuario hace click en el boton AddToCart
    And el usuario acepta la alerta
    And el usuario hace click en el boton Cart
    And el usuario verifica el monto a pagar
    And el usuario hace click en el boton PlaceOrder
    And el usuario ingresa los campos para pagar. Name: <name> Country: <country> City:<city> CreditCard: <card> Month <month> Year: <year>
    And el usuario hace click en el boton Pucharse
    Then el usuario verifica el mensaje de compra valida
    Examples:
      | id | secondId | name | country | city | card      | month | year |
      | 1  | 5        | jc   | arg     | mdz  | 342141234 | 02    | 2025 |

