package com.autoc0de.steps;

import com.autoc0de.pages.EccomercePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class EccomerceSteps {

    /*
     ** PAGE INSTANCE
     */

    EccomercePage eccomercePage = new EccomercePage();


    @When("^el usuario hace click en el boton (.*)")
    public void elUsuarioHaceClickEnElBoton(String name) {
        eccomercePage.clickButtonSwitch(name);
    }

    @Then("^el usuario verifica el (.*) de alerta$")
    public void elUsuarioVerificaElMensajeDeAlerta(String message) {
        eccomercePage.verifyAlertSingle(message);
    }

    @And("el usuario acepta la alerta")
    public void elUsuarioAceptaLaAlerta() {
        eccomercePage.acceptAlert();
    }

    @Given("el usuario esta en el home de demoblaze.com")
    public void elUsuarioEstaEnElHomeDeDemoblazeCom() {
        eccomercePage.verifyHomeTitle();
    }

    @And("^el usuario ingresa los campos. Username: (.*) Password: (.*)$")
    public void elUsuarioCompletaLosCamposUsernameUserPasswordPass(String user, String pass) {
        eccomercePage.completeSingUpData(user, pass);
    }

    @And("^el usuario ingresa los campos login. Username: (.*) Password: (.*)$")
    public void elUsuarioCompletaLosCamposLoginUsernameUserPasswordPass(String user, String pass) {

        eccomercePage.completeLoginData(user, pass);
    }

    @Then("^el usuario verifica la presencia de : (.*) de la sesion$")
    public void elUsuarioVerificaLaPresenciaDeUserDeLaSesion(String user) {
        eccomercePage.verifySessionName(user);

    }

    @When("^el usuario hace click en el item: (.*)$")
    public void elUsuarioHaceClickEnElItemId(String id) {
        eccomercePage.clickOnItemToAddByIdTable(id);
    }

    @And("^el usuario hace click en un adicional item: (.*)$")
    public void elUsuarioHaceClickEnElSegundoItemId(String id) {
        eccomercePage.clickOnItemToAddByIdTable(id);
    }

    @And("el usuario verifica el monto a pagar")
    public void elUsuarioVerificaElMontoAPagar() {
        eccomercePage.sumTotalPriceToItemsCart();
    }

    @And("^el usuario ingresa los campos para pagar. Name: (.*) Country: (.*) City:(.*) CreditCard: (.*) Month (.*) Year: (.*)$")
    public void elUsuarioIngresaLosCamposParaPagarNameNameCountryCountryCityCityCreditCardCardMonthMonthYearYear(String name, String country, String city, String card, String month, String year) {
        eccomercePage.completeCardData(name, country, city, card, month, year);
    }

    @Then("el usuario verifica el mensaje de compra valida")
    public void elUsuarioVerificaElMensajeDeCompraValida() {
        eccomercePage.verifyTitleOperationCard();
    }
}
