package com.autoc0de.pages;


import com.autoc0de.core.utility.MasterPage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;


import static com.autoc0de.core.utility.Utils.getRandomLetters;

public class EccomercePage extends MasterPage {

    SoftAssert softAssert = new SoftAssert();

    /*
     ** CONSTANTS **
     */

    private final String HOME_TITLE_XPATH = "//nav[@id='narvbarx']//a";

    private final String HOME_NAV_XPATH = "//a[@class='nav-link']";

    private final String SINGUP_BUTTON_ID = "signin2";

    private final String NEXTPAG_BUTTON_ID = "next2";

    private final String PREVPAG_BUTTON_ID = "prev2";

    private final String BASE_PRODUCT_TO_ADD_XPATH = "(//a[@class='hrefch'])";

    private final String ADD_ITEM_CART_BUTTON_LINKTEXT = "Add to cart";

    private final String CART_NAV_ID = "cartur";

    private final String PLACE_ORDER_BUTTON_XPATH = "//button[text()='Place Order']";

    private final String PLACE_ORDER_NAME_ID = "name";

    private final String PLACE_ORDER_COUNTRY_ID = "country";

    private final String PLACE_ORDER_CITY_ID = "city";

    private final String PLACE_ORDER_CARD_ID = "card";

    private final String PLACE_ORDER_MONTH_CARD_ID = "month";

    private final String PLACE_ORDER_YEAR_CARD_ID = "year";

    private final String H2_PURCHARSE_TITLE_XPATH = "//h2[text()='Thank you for your purchase!']";

    private final String PURCHARSE_BUTTON_XPATH = "//button[text()='Purchase']";
    private final String TOTAL_PRICE_ID = "totalp";

    private final String ITEM_CART_TABLE_XPATH = "//table[contains(@class,'table table-bordered')]";

    private final String CONTACT_NAV_LINKTEXT = "Contact";

    private final String CONTACT_MAIL_ID = "recipient-email";

    private final String CONTACT_NAME_ID = "recipient-name";

    private final String CONTACT_MESSAGE_ID = "message-text";

    private final String SEND_MESSAGE_BUTTON_XPATH = "//button[text()='Send message']";

    private final String LOGIN_BUTTON_ID = "login2";

    private final String USERNAME_SESSION_ID = "nameofuser";

    private final String LOGIN_MODAL_BUTTON_XPATH = "//button[text()='Log in']";

    private final String USERNAME_INPUT_LOGIN_ID = "loginusername";

    private final String PASS_INPUT_LOGIN_ID = "loginpassword";
    private final String SINGUP_MODAL_BUTTONS_XPATH = "//button[text()='Sign up']";
    private final String USERNAME_INPUT_SINGUP_ID = "sign-username";
    private final String PASS_INPUT_SINGUP_ID = "sign-password";


    /*
     ** //FUNCTIONS **
     */

    public void clickButtonSwitch(String button) {
        switch (button.toLowerCase()) {
            case "home":
                clickOnHomeNav();
                break;
            case "singup":
                clickOnSingUpButtons();
                break;
            case "singupmodal":
                clickOnSingUpButtonsModal();
                break;
            case "login":
                clickOnLoginButtons();
                break;
            case "loginmodal":
                clickOnLoginButtonsModal();
                break;
            case "contact":
                clickOnContactButtons();
                break;
            case "sendmessage":
                clickOnSendMessageButtons();
                break;
            case "cart":
                clickOnCartNav();
                break;
            case "addtocart":
                clickOnAddToCart();
                break;
            case "placeorder":
                clickOnPlaceOrder();
                break;
            case "pucharse":
                clickOnPucharse();
                break;
            case "nextpag":
                clickOnNextPag();
                break;
            case "prevpag":
                clickOnPrevPag();
                break;
            default:
                Assert.assertEquals(button, "No button matched", "Invalid button options");
        }
    }

    public void sumTotalPriceToItemsCart() {
        int countRows = auto_getRowsTable(By.xpath(ITEM_CART_TABLE_XPATH)).size();
        int sum = 0;

        for (int i = 1; i <= countRows + 1; i++) {
            String value = auto_getWebElement(By.xpath(ITEM_CART_TABLE_XPATH + "/tbody[1]/tr[" + (i) + "]/td[3]")).getText();
            sum += Integer.parseInt(value);
        }
        Assert.assertEquals(Integer.parseInt(auto_getWebElement(By.id(TOTAL_PRICE_ID)).getText()), sum, "total price error");
    }

    public void clickOnPrevPag() {
        auto_setClickElement(By.id(PREVPAG_BUTTON_ID));
    }

    public void clickOnNextPag() {
        auto_setClickElement(By.id(NEXTPAG_BUTTON_ID));
    }

    public void clickOnCartNav() {
        auto_setClickElement(By.id(CART_NAV_ID));
    }

    public void clickOnAddToCart() {
        auto_setClickElement(By.linkText(ADD_ITEM_CART_BUTTON_LINKTEXT));
        auto_getWaitInMiliseconds(1000);

    }

    public void clickOnHomeNav() {
        auto_setClickElement(By.xpath(HOME_NAV_XPATH));
    }

    public void clickOnItemToAdd() {
        auto_setClickElement(By.xpath(BASE_PRODUCT_TO_ADD_XPATH));
    }

    public void clickOnItemToAddByIdTable(String idProduct) {
        auto_setClickElement(By.xpath(BASE_PRODUCT_TO_ADD_XPATH + "[" + idProduct + "]"));
    }

    public void clickOnPlaceOrder() {
        auto_setClickElement(By.xpath(PLACE_ORDER_BUTTON_XPATH));
    }

    public void clickOnContactButtons() {
        auto_setClickElement(By.linkText(CONTACT_NAV_LINKTEXT));
    }

    public void clickOnPucharse() {
        auto_setClickElement(By.xpath(PURCHARSE_BUTTON_XPATH));
    }

    public void clickOnSendMessageButtons() {
        auto_setClickElement(By.xpath(SEND_MESSAGE_BUTTON_XPATH));
    }

    public void clickOnSingUpButtons() {
        auto_setClickElement(By.id(SINGUP_BUTTON_ID));
    }

    public void clickOnLoginButtonsModal() {
        auto_setClickElement(By.xpath(LOGIN_MODAL_BUTTON_XPATH));
        auto_getWaitInMiliseconds(1000);
        auto_isElementPresent(By.id(USERNAME_SESSION_ID));

    }

    public void clickOnLoginButtons() {
        auto_setClickElement(By.id(LOGIN_BUTTON_ID));
    }

    public void clickOnSingUpButtonsModal() {
        auto_setClickElement(By.xpath(SINGUP_MODAL_BUTTONS_XPATH));
        auto_getWaitInMiliseconds(1000);

    }

    public void completeLoginData(String user, String pass) {

        auto_waitElementClickable(By.id(USERNAME_INPUT_LOGIN_ID));
        auto_setTextToInput(By.id(USERNAME_INPUT_LOGIN_ID), user);
        auto_setTextToInput(By.id(PASS_INPUT_LOGIN_ID), pass);

    }

    public void completeCardData(String name, String country, String city, String card, String month, String year) {
        auto_waitElementClickable(By.id(PLACE_ORDER_NAME_ID));
        auto_setTextToInput(By.id(PLACE_ORDER_NAME_ID), name);
        auto_setTextToInput(By.id(PLACE_ORDER_COUNTRY_ID), country);
        auto_setTextToInput(By.id(PLACE_ORDER_CITY_ID), city);
        auto_setTextToInput(By.id(PLACE_ORDER_CARD_ID), card);
        auto_setTextToInput(By.id(PLACE_ORDER_MONTH_CARD_ID), month);
        auto_setTextToInput(By.id(PLACE_ORDER_YEAR_CARD_ID), year);
    }

    public void completeContactData(String contactEmail, String contactName, String message) {
        auto_waitElementClickable(By.id(USERNAME_INPUT_LOGIN_ID));
        auto_setTextToInput(By.id(CONTACT_MAIL_ID), contactEmail);
        auto_setTextToInput(By.id(CONTACT_NAME_ID), contactName);
        auto_setTextToInput(By.id(CONTACT_MESSAGE_ID), message);
    }

    public void completeSingUpData(String user, String pass) {

        if (!user.equals("juancruzmunozalbelotest@gmail.com")) user += getRandomLetters(5);
        auto_waitElementClickable(By.id(USERNAME_INPUT_SINGUP_ID));
        auto_setTextToInput(By.id(USERNAME_INPUT_SINGUP_ID), user);
        auto_setTextToInput(By.id(PASS_INPUT_SINGUP_ID), pass);
    }

    public void verifyTitleOperationCard() {
        Assert.assertTrue(auto_isElementVisible(By.xpath(H2_PURCHARSE_TITLE_XPATH)));
    }

    public void verifyHomeTitle() {
        Assert.assertTrue(auto_isElementVisible(By.xpath(HOME_TITLE_XPATH)));
    }

    public void verifySessionName(String user) {
        auto_getWaitInMiliseconds(1000);
        Assert.assertTrue(auto_getElementText(By.id(USERNAME_SESSION_ID)).contains(user), "error at session");
    }

    public void verifyAlertSingle(String messageAlert) {
        Assert.assertTrue(auto_GetAlert().getText().contains(messageAlert), "Error at Alert");
    }

    public void acceptAlert() {
        auto_GetAlert().accept();
    }

}
